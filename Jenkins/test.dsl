pipelineJob('test-job') {

 triggers {
   gitlab{
     secretToken("abc123")
     triggerOnNoteRequest(false)
   }
 }


  definition {
    cps {
        script('''
        pipeline {
            agent any
                stages {
                    stage('Check out ') {
                        steps {
                            git branch: 'dev',
                                url:    'http://gitlab.example.com/jefwang/hellocicd.git',
                                credentialsId: '59af1395-8894-4311-b063-dfdba93f471d'
                        }
                    }
                    stage('Build') {
                        steps {
                            echo 'Build'
                            sh 'mvn package -Dmaven.test.skip'
                        }
                    }
                    stage('Test') {
                        steps {
                            sh 'mvn test'
                        }
                    }
                }
                post{
                  always{
                    mail to: 'test@aldi.com',
                    subject: "Status of pipeline: ${currentBuild.fullDisplayName}",
                    body: "${env.BUILD_URL} has result ${currentBuild.result}"
                  }
                }
        }
      '''.stripIndent())
      sandbox()     
    }
  }
}