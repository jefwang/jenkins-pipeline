pipelineJob('master-job') {


  triggers {
    gitlab{
      triggerOnPush(false)
      triggerOnMergeRequest(false)
      triggerOnAcceptedMergeRequest(true)
      triggerOnClosedMergeRequest(false)
      triggerOpenMergeRequestOnPush("never")
      triggerOnNoteRequest(true)
      noteRegex( "Jenkins please retry a build")
      skipWorkInProgressMergeRequest( true)
      ciSkip( false)
      setBuildDescription( true)
      addNoteOnMergeRequest( true)
      addCiMessage( true)
      addVoteOnMergeRequest( true)
      acceptMergeRequestOnSuccess( false)
      pendingBuildName( "Jenkins")
      cancelPendingBuildsOnUpdate( false)
      secretToken( "abc123")
    }
  }


  definition {
    cps {
        script('''
        def  DOCKER_REGISTRY='https://harbor.run.haas-206.pez.pivotal.io'
        def  DOCKER_CRED='59af1395-8894-4311-b063-dfdba93f471d'
        def  IMAGE_VERSION = 1
        def  myimage
        pipeline {
            agent any
                stages {
                    stage('Check out ') {
                        steps {
                            git branch: 'master',
                                url:    'http://gitlab.example.com/jefwang/hellocicd.git',
                                credentialsId: '59af1395-8894-4311-b063-dfdba93f471d'
                        }
                    }
                    stage('Build artifact') {
                        steps {
                            echo 'Build stage'
                            sh 'mvn package -Dmaven.test.skip'
                        }
                    }
                    stage('Test') {
                        steps {
                            sh '''
                                env
                                pwd
                            '''
                        }
                    }

                    stage('Build image') {
                        steps {
                            script{
                                myimage = docker.build("pksdemo/myimg")
                            }

                        }
                    }

                    stage('Test image') {
                        steps {
                            echo 'run testing'
                            script{
                                /***
                                myimage.inside{
                                  sh 'env'
                                }
                                */
                                sh 'ls'

                            }
                        }
                    }
                    stage('Upload image') {
                        steps {
                            script{
                                docker.withRegistry(DOCKER_REGISTRY, DOCKER_CRED){
                                    myimage.push("${IMAGE_VERSION}.${env.BUILD_NUMBER}")
                                    myimage.push("int.candidate")
                                }
                            }
                        }
                    }

                }
        }
      '''.stripIndent())
      sandbox()
    }
  }
}